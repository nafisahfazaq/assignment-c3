# **ARTME**

**Table of Content:**

- [ARTME](#artme)
  - [**Problems and Solutions**](#problems-and-solutions)
  - [**User Flow and User Story**](#user-flow-and-user-story)
  - [**Tabel Diagram and API Contract**](#tabel-diagram-and-api-contract)
    - [**Tabel Diagram**](#tabel-diagram)
    - [**API Contract**](#api-contract)
  - [**User interface**](#user-interface)
  - [**Source code**](#source-code)
  - [**To do list**](#to-do-list)

## **Problems and Solutions**

- Banyak orang yang mau memberikan hadiah berupa gambar atau **membutuhkan jasa artist** tapi **kesulitan menemukan artist yang styelnya cocok** dengan mereka dan **dengan biaya yang mereka sanggupi**
> **Solution :** Fitur search by style dan rentang harga

- Banyak **Artist kesulitan mencari customer**, sementara sumber biaya hidupnya dari commission
> **Solution :** Platform dengan cakupan luas tanpa algoritma *“akun besar semakin terkenal, akun kecil need to struggling more”.* ~~*Tapi gacha :D alias munculnya di customer random  (hehe)*~~

- Banyak Artist yang masih **kurang paham dalam pembuatan term of service**
> **Solution :** Term of Service sudah ditentukan di awal

- ~~*(bonus :D)*~~ Banyak Artist yang **bingung dengan rate commish** mereka, apakah mereka mengatur harga commish mereka terlalu tinggi atau terlalu rendah atau sudah cukup
> **Solution :** Artist dapat melihat rate Artist lain yang serupa dengan style mereka sehingga dapat menentukan kelayakan harga mereka

---

## **User Flow and User Story**

![User Flow](userflow.png)

| Epic | User | User Story | Task |
| ----------- | ----------- | ----------- | ----------- |
| Register | Artist and Commissioner | As an Artist/Commissioner I want to register to make an account | UI&FE: create register page |
|  |  |  | UI&FE: create login page |
|  |  |  | BE : Create user database : ID, name, email, password, instagram, twitter |
|  |  |  | FE : integrated with API |
|  |  | As an Artist/Commissioner I want to change my account information | UI&FE : create profile page |
|  |  |  | UI&FE : create profile form to change data |
|  |  |  | BE : create update data |
| Order a commission | Commissioner | As a commissioner, I want to find a suitable artist, so I can order arts for my needs | Create search feature based on style (card for style) |
|  |  |  | Create list of commission (based on artist) |
|  |  |  | Create page Artist (contain artist profile and their list of commission) |
|  |  | As a commissioner, I want to find a suitable price for my order, so I can order an art according to my budget | Create slider for price range |
|  |  |  | *next go to list of commission based on price range |
|  |  | As a commissioner I want to track my order, so I can see the progress of my order and my order history in case I want to make repeated order with the same artist | Create order history page |
|  |  |  | Get order data from ID customer |
|  |  |  | Create page order summary (per order) |
|  |  | As a Commissioner, I want to make an order based on the type I choose | create form page contain info about the type, info about payment method, place to upload payment proof |
|  | Artist | As an Artist, I want to get a payment information, so I can confirm my Commissioner payment | go to page order summary from order list history |
|  |  |  | notification for artist after commissioner upload the payment proof |
|  |  | As an Artist, I want to inform my Commissioner about their commission status, so I can update the status and discuss more about it with them | add edit status (dropdown), upload image for progress), and comment text field (like chat-feature) in order summary |
|  |  | As an Artist I want to know my Commissioner ‘s social media, so when I upload my comms work to my socmed, I can tag them | Link commissioner name to commissioner profile in order summary/history list |
|  |  | As an Artist I want to track my order list, so I can track my total sales | create page order history |
| See List commission | Artist | As an Artist I want to create my commission list, so Commissioner can view my offerings | create page add/edit commission list |
|  | Commissioner | As a Commissioner, I want to see the list of the types of art an Artist offers | create page artist’s commission list info |
| Security | Admin | As an Admin, I want to make the terms of service for Artist and Commissioner, so they can transact with clear rules | create page terms of service |
|  |  | As an Admin I want to track Artist and Commissioner account, so I can know if there’re unusual activity such as fraud etc. | create dashboard management |
|  |  | As an Admin, I want to blocked user if they violated the rules | give permission to change status in deleted_at at user’s tabel |

---

## **Tabel Diagram and API Contract**

### **Tabel Diagram**
![ERD](tabel-diagram.png)

### **API Contract**
![API_Contract](api-contract.png)

---

## **User interface**

[Go to Figma](https://www.figma.com/proto/xsyHDLMlnlHmwJlAUX0csa/ArtMe?page-id=49%3A1109&node-id=51%3A1020&viewport=440%2C940%2C0.29&scaling=scale-down-width&starting-point-node-id=51%3A1020)

---

## **Source code**

[Project source code](https://gitlab.com/nafisahfazaq/assignment-c3.git)

---

## **To do list**

- [x] Project description with Markdown
- [x] Kanban Management Tool with Notion
- [x] Design with Figma
- [x] ERD and API contract with Whimsical
- [x] Database with Supabase
- [ ] Boilerplate clean architecture with Golang bang daud
- [ ] Management dashboard with Retool
- [ ] Endpoint Retool with Supabase
- [ ] Endpoint frontend with Golang
- [ ] Frontend with Vue JS
- [ ] Automation test with Postman
- [ ] Automation test with Cyrpess
- [ ] Deployment with Heroku