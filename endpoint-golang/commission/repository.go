package commission

import "gorm.io/gorm"

type Repository interface {
	FindAll() ([]Commission, error)
	FindByID(ID int) (Commission, error)
	Create(commission Commission) (Commission, error)
	Update(commission Commission) (Commission, error)
	Delete(commission Commission) (Commission, error)
}

// implementation (private so the title lowcase)
type repository struct {
	db *gorm.DB
	//change the database
}

// new repo
func NewRepository(db *gorm.DB) *repository {
	return &repository{db}
}

// read data
func (r *repository) FindAll() ([]Commission, error) {
	var commissions []Commission
	err := r.db.Find(&commissions).Error
	return commissions, err
}

// search data
func (r *repository) FindByID(ID int) (Commission, error) {
	var commission Commission
	err := r.db.Find(&commission, ID).Error
	return commission, err
}

// create data
func (r *repository) Create(commission Commission) (Commission, error) {
	err := r.db.Create(&commission).Error
	return commission, err
}

// update data
func (r *repository) Update(commission Commission) (Commission, error) {
	err := r.db.Save(&commission).Error
	return commission, err
}

// delete data
func (r *repository) Delete(commission Commission) (Commission, error) {
	err := r.db.Delete(&commission).Error
	return commission, err
}
