package commission

type Service interface {
	FindAll() ([]Commission, error)
	FindByID(ID int) (Commission, error)
	Create(commissionInput CommissionInput) (Commission, error)
	Update(ID int, commissionInput CommissionInput) (Commission, error)
	Delete(ID int) (Commission, error)
}

type service struct {
	repository Repository
}

// new service
func NewService(repository Repository) *service {
	return &service{repository}
}

// read data
func (s *service) FindAll() ([]Commission, error) {
	commissions, err := s.repository.FindAll()
	return commissions, err
}

// search data
func (s *service) FindByID(ID int) (Commission, error) {
	commission, err := s.repository.FindByID(ID)
	return commission, err
}

// create data
func (s *service) Create(commissionInput CommissionInput) (Commission, error) {
	commission := Commission{
		ID:         commissionInput.ID,
		ID_User:    commissionInput.ID_User,
		Image_URL:  commissionInput.Image_URL,
		Title:      commissionInput.Title,
		Style:      commissionInput.Style,
		Created_at: commissionInput.Created_at,
		Updated_at: commissionInput.Updated_at,
		Deleted_at: commissionInput.Deleted_at,
	}

	newCommission, err := s.repository.Create(commission)
	return newCommission, err
}

// update data
func (s *service) Update(ID int, commissionInput CommissionInput) (Commission, error) {
	commission, _ := s.repository.FindByID(ID)

	commission.Image_URL = commissionInput.Image_URL
	commission.Title = commissionInput.Title
	commission.Style = commissionInput.Style
	commission.Created_at = commissionInput.Created_at
	commission.Updated_at = commissionInput.Updated_at
	commission.Deleted_at = commissionInput.Deleted_at

	newCommission, err := s.repository.Update(commission)
	return newCommission, err
}

// update data
func (s *service) Delete(ID int) (Commission, error) {
	commission, _ := s.repository.FindByID(ID)
	newCommission, err := s.repository.Delete(commission)
	return newCommission, err
}
