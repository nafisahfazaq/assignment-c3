package commission

import "time"

// struct for get user
type CommissionResponse struct {
	ID         int
	ID_User    int
	Image_URL  string `json:"image" binding:"required"`
	Title      string `json:"title" binding:"required"`
	Style      string `json:"style" binding:"required"`
	Created_at time.Time
	Updated_at time.Time
	Deleted_at time.Time
}
