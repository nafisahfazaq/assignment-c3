package commission

import "time"

type Commission struct {
	ID         int
	ID_User    int
	Image_URL  string
	Title      string
	Style      string
	Created_at time.Time
	Updated_at time.Time
	Deleted_at time.Time
}
