package subtype

import "gorm.io/gorm"

type Repository interface {
	FindAll() ([]Subtype, error)
	FindByID(ID int) (Subtype, error)
	Create(subtype Subtype) (Subtype, error)
	Update(subtype Subtype) (Subtype, error)
	Delete(subtype Subtype) (Subtype, error)
}

// implementation (private so the title lowcase)
type repository struct {
	db *gorm.DB
	//change the database
}

// new repo
func NewRepository(db *gorm.DB) *repository {
	return &repository{db}
}

// read data
func (r *repository) FindAll() ([]Subtype, error) {
	var subtypes []Subtype
	err := r.db.Find(&subtypes).Error
	return subtypes, err
}

// search data
func (r *repository) FindByID(ID int) (Subtype, error) {
	var subtype Subtype
	err := r.db.Find(&subtype, ID).Error
	return subtype, err
}

// create data
func (r *repository) Create(subtype Subtype) (Subtype, error) {
	err := r.db.Create(&subtype).Error
	return subtype, err
}

// update data
func (r *repository) Update(subtype Subtype) (Subtype, error) {
	err := r.db.Save(&subtype).Error
	return subtype, err
}

// delete data
func (r *repository) Delete(subtype Subtype) (Subtype, error) {
	err := r.db.Delete(&subtype).Error
	return subtype, err
}
