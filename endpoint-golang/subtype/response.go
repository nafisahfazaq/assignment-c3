package subtype

import "time"

// struct for get user
type SubtypeResponse struct {
	ID         int
	ID_User    int
	Title      string `json:"title" binding:"required"`
	Price      int    `json:"price" binding:"required, number"`
	Created_at time.Time
	Updated_at time.Time
	Deleted_at time.Time
}
