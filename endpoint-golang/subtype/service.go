package subtype

type Service interface {
	FindAll() ([]Subtype, error)
	FindByID(ID int) (Subtype, error)
	Create(subtypeInput SubtypeInput) (Subtype, error)
	Update(ID int, subtypeInput SubtypeInput) (Subtype, error)
	Delete(ID int) (Subtype, error)
}

type service struct {
	repository Repository
}

// new service
func NewService(repository Repository) *service {
	return &service{repository}
}

// read data
func (s *service) FindAll() ([]Subtype, error) {
	subtypes, err := s.repository.FindAll()
	return subtypes, err
}

// search data
func (s *service) FindByID(ID int) (Subtype, error) {
	subtype, err := s.repository.FindByID(ID)
	return subtype, err
}

// create data
func (s *service) Create(subtypeInput SubtypeInput) (Subtype, error) {
	price, _ := subtypeInput.Price.Int64()
	subtype := Subtype{
		ID:         subtypeInput.ID,
		ID_User:    subtypeInput.ID_User,
		Title:      subtypeInput.Title,
		Price:      int(price),
		Created_at: subtypeInput.Created_at,
		Updated_at: subtypeInput.Updated_at,
		Deleted_at: subtypeInput.Deleted_at,
	}

	newSubtype, err := s.repository.Create(subtype)
	return newSubtype, err
}

// update data
func (s *service) Update(ID int, subtypeInput SubtypeInput) (Subtype, error) {
	subtype, _ := s.repository.FindByID(ID)
	price, _ := subtypeInput.Price.Int64()

	subtype.Title = subtypeInput.Title
	subtype.Price = int(price)
	subtype.Created_at = subtypeInput.Created_at
	subtype.Updated_at = subtypeInput.Updated_at
	subtype.Deleted_at = subtypeInput.Deleted_at

	newSubtype, err := s.repository.Update(subtype)
	return newSubtype, err
}

// update data
func (s *service) Delete(ID int) (Subtype, error) {
	subtype, _ := s.repository.FindByID(ID)
	newSubtype, err := s.repository.Delete(subtype)
	return newSubtype, err
}
