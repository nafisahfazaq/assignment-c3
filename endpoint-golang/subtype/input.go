package subtype

import (
	"encoding/json"
	"time"
)

// struct for post user
type SubtypeInput struct {
	ID         int
	ID_User    int
	Title      string      `json:"title" binding:"required"`
	Price      json.Number `json:"price" binding:"required, number"`
	Created_at time.Time
	Updated_at time.Time
	Deleted_at time.Time
}
