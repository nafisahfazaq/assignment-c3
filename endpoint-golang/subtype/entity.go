package subtype

import "time"

type Subtype struct {
	ID         int
	ID_User    int
	Title      string
	Price      int
	Created_at time.Time
	Updated_at time.Time
	Deleted_at time.Time
}
