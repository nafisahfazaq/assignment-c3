package order

import "gorm.io/gorm"

type Repository interface {
	FindAll() ([]Order, error)
	FindByID(ID int) (Order, error)
	Create(order Order) (Order, error)
	Update(order Order) (Order, error)
	Delete(order Order) (Order, error)
}

// implementation (private so the title lowcase)
type repository struct {
	db *gorm.DB
	//change the database
}

// new repo
func NewRepository(db *gorm.DB) *repository {
	return &repository{db}
}

// read data
func (r *repository) FindAll() ([]Order, error) {
	var orders []Order
	err := r.db.Find(&orders).Error
	return orders, err
}

// search data
func (r *repository) FindByID(ID int) (Order, error) {
	var order Order
	err := r.db.Find(&order, ID).Error
	return order, err
}

// create data
func (r *repository) Create(order Order) (Order, error) {
	err := r.db.Create(&order).Error
	return order, err
}

// update data
func (r *repository) Update(order Order) (Order, error) {
	err := r.db.Save(&order).Error
	return order, err
}

// delete data
func (r *repository) Delete(order Order) (Order, error) {
	err := r.db.Delete(&order).Error
	return order, err
}
