package order

type Service interface {
	FindAll() ([]Order, error)
	FindByID(ID int) (Order, error)
	Create(orderInput OrderInput) (Order, error)
	Update(ID int, orderInput OrderInput) (Order, error)
	Delete(ID int) (Order, error)
}

type service struct {
	repository Repository
}

// new service
func NewService(repository Repository) *service {
	return &service{repository}
}

// read data
func (s *service) FindAll() ([]Order, error) {
	orders, err := s.repository.FindAll()
	return orders, err
}

// search data
func (s *service) FindByID(ID int) (Order, error) {
	order, err := s.repository.FindByID(ID)
	return order, err
}

// create data
func (s *service) Create(orderInput OrderInput) (Order, error) {
	order := Order{
		ID:              orderInput.ID,
		ID_Artist:       orderInput.ID_Artist,
		ID_Commissioner: orderInput.ID_Commissioner,
		ID_Subtype:      orderInput.ID_Subtype,
		Payment_proof:   orderInput.Payment_proof,
		Status:          orderInput.Status,
		Date_Ordered:    orderInput.Date_Ordered,
		Updated_at:      orderInput.Updated_at,
		Date_Finished:   orderInput.Date_Finished,
		Deleted_at:      orderInput.Deleted_at,
	}

	newOrder, err := s.repository.Create(order)
	return newOrder, err
}

// update data
func (s *service) Update(ID int, orderInput OrderInput) (Order, error) {
	order, _ := s.repository.FindByID(ID)

	order.Payment_proof = orderInput.Payment_proof
	order.Status = orderInput.Status
	order.Date_Finished = orderInput.Date_Finished

	newOrder, err := s.repository.Update(order)
	return newOrder, err
}

// update data
func (s *service) Delete(ID int) (Order, error) {
	order, _ := s.repository.FindByID(ID)
	newOrder, err := s.repository.Delete(order)
	return newOrder, err
}
