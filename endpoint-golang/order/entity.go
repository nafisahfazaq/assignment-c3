package order

import "time"

type Order struct {
	ID              int
	ID_Artist       int
	ID_Commissioner int
	ID_Subtype      int
	Payment_proof   string
	Status          string
	Date_Ordered    time.Time
	Updated_at      time.Time
	Date_Finished   time.Time
	Deleted_at      time.Time
}
