package order

import "time"

// struct for get user
type OrderResponse struct {
	ID              int
	ID_Artist       int
	ID_Commissioner int
	ID_Subtype      int
	Payment_proof   string `json:"title" binding:"required"`
	Status          string
	Date_Ordered    time.Time
	Updated_at      time.Time
	Date_Finished   time.Time
	Deleted_at      time.Time
}
