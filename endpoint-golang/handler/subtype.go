package handler

import (
	"fmt"
	"golang-api/subtype"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	//"gitlab.com/nafisahfazaq/assignment-c3/-/tree/main/endpoint-golang/subtype"
	//"endpoint-golang/subtype"
)

type subtypeHandler struct {
	subtypeService subtype.Service
}

func NewSubtypeHandler(subtypeService subtype.Service) *subtypeHandler {
	return &subtypeHandler{subtypeService}
}

// endpoint get subtype list
func (handler *subtypeHandler) GetSubtypes(c *gin.Context) {

	subtypes, err := handler.subtypeService.FindAll()
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err,
		})
		return
	}

	//endpoint get subtype list #2
	var subtypesResponse []subtype.SubtypeResponse
	for _, s := range subtypes {
		subtypeResponse := convertToSubtypeResponse(s)
		subtypesResponse = append(subtypesResponse, subtypeResponse)
	}

	c.JSON(http.StatusOK, gin.H{
		"data": subtypesResponse,
	})
}

// endpoint get single subtype
func (handler *subtypeHandler) GetSubtype(c *gin.Context) {
	idString := c.Param("id")
	id, _ := strconv.Atoi(idString)

	s, err := handler.subtypeService.FindByID(int(id))

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err,
		})
		return
	}

	subtypeResponse := convertToSubtypeResponse(s)

	c.JSON(http.StatusOK, gin.H{
		"data": subtypeResponse,
	})
}

// endpoint update
func (handler *subtypeHandler) UpdateSubtype(c *gin.Context) {
	var subtypeInput subtype.SubtypeInput

	err := c.ShouldBindJSON(&subtypeInput)
	if err != nil {
		//data validation

		errorMessages := []string{}
		for _, e := range err.(validator.ValidationErrors) {
			errorMessage := fmt.Sprintf("Error on field %s, condition %s", e.Field(), e.ActualTag())
			errorMessages = append(errorMessages, errorMessage)
		}

		c.JSON(http.StatusBadRequest, gin.H{
			"error": errorMessages,
		})
		return
	}

	idString := c.Param("id")
	id, _ := strconv.Atoi(idString)
	subtype, err := handler.subtypeService.Update(id, subtypeInput)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err,
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"data": convertToSubtypeResponse(subtype),
	})
}

// endpoint delete
func (handler *subtypeHandler) DeleteSubtype(c *gin.Context) {
	idString := c.Param("id")
	id, _ := strconv.Atoi(idString)
	s, err := handler.subtypeService.Delete(int(id))

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err,
		})
		return
	}

	subtypeResponse := convertToSubtypeResponse(s)

	c.JSON(http.StatusOK, gin.H{
		"data": subtypeResponse,
	})
}

// add data
func (handler *subtypeHandler) CreateSubtype(c *gin.Context) {
	var subtypeInput subtype.SubtypeInput

	err := c.ShouldBindJSON(&subtypeInput)
	if err != nil {
		//data validation

		errorMessages := []string{}
		for _, e := range err.(validator.ValidationErrors) {
			errorMessage := fmt.Sprintf("Error on field %s, condition %s", e.Field(), e.ActualTag())
			errorMessages = append(errorMessages, errorMessage)
		}

		c.JSON(http.StatusBadRequest, gin.H{
			"error": errorMessages,
		})
		return
	}

	//call the service
	subtype, err := handler.subtypeService.Create(subtypeInput)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err,
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"data": convertToSubtypeResponse(subtype),
	})
}

func convertToSubtypeResponse(s subtype.Subtype) subtype.SubtypeResponse {
	return subtype.SubtypeResponse{
		ID:         s.ID,
		ID_User:    s.ID_User,
		Title:      s.Title,
		Price:      s.Price,
		Created_at: s.Created_at,
		Updated_at: s.Updated_at,
		Deleted_at: s.Deleted_at,
	}
}
