package handler

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"

	//"gitlab.com/nafisahfazaq/assignment-c3/-/tree/main/endpoint-golang/user"
	"golang-api/user"
)

type userHandler struct {
	userService user.Service
}

func NewUserHandler(userService user.Service) *userHandler {
	return &userHandler{userService}
}

// endpoint get userlist
func (handler *userHandler) Getusers(c *gin.Context) {

	users, err := handler.userService.FindAll()
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err,
		})
		return
	}

	//endpoint get userlist #2
	var usersResponse []user.UserResponse
	for _, u := range users {
		userResponse := convertToUserResponse(u)
		usersResponse = append(usersResponse, userResponse)
	}

	c.JSON(http.StatusOK, gin.H{
		"data": usersResponse,
	})
}

// endpoint get single user
func (handler *userHandler) GetUser(c *gin.Context) {
	idString := c.Param("id")
	id, _ := strconv.Atoi(idString)

	u, err := handler.userService.FindByID(int(id))

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err,
		})
		return
	}

	bookResponse := convertToUserResponse(u)

	c.JSON(http.StatusOK, gin.H{
		"data": bookResponse,
	})
}

// endpoint update
func (handler *userHandler) UpdateUser(c *gin.Context) {
	var userInput user.UserInput

	err := c.ShouldBindJSON(&userInput)
	if err != nil {
		//data validation

		errorMessages := []string{}
		for _, e := range err.(validator.ValidationErrors) {
			errorMessage := fmt.Sprintf("Error on field %s, condition %s", e.Field(), e.ActualTag())
			errorMessages = append(errorMessages, errorMessage)
		}

		c.JSON(http.StatusBadRequest, gin.H{
			"error": errorMessages,
		})
		return
	}

	idString := c.Param("id")
	id, _ := strconv.Atoi(idString)
	user, err := handler.userService.Update(id, userInput)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err,
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"data": convertToUserResponse(user),
	})
}

// endpoint delete
func (handler *userHandler) DeleteUser(c *gin.Context) {
	idString := c.Param("id")
	id, _ := strconv.Atoi(idString)
	u, err := handler.userService.Delete(int(id))

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err,
		})
		return
	}

	userResponse := convertToUserResponse(u)

	c.JSON(http.StatusOK, gin.H{
		"data": userResponse,
	})
}

// add data
func (handler *userHandler) CreateUser(c *gin.Context) {
	var userInput user.UserInput

	err := c.ShouldBindJSON(&userInput)
	if err != nil {
		//data validation

		errorMessages := []string{}
		for _, e := range err.(validator.ValidationErrors) {
			errorMessage := fmt.Sprintf("Error on field %s, condition %s", e.Field(), e.ActualTag())
			errorMessages = append(errorMessages, errorMessage)
		}

		c.JSON(http.StatusBadRequest, gin.H{
			"error": errorMessages,
		})
		return
	}

	//call the service
	user, err := handler.userService.Create(userInput)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err,
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"data": convertToUserResponse(user),
	})
}

func convertToUserResponse(u user.User) user.UserResponse {
	return user.UserResponse{
		ID:          u.ID,
		Name:        u.Name,
		Email:       u.Email,
		Password:    u.Password,
		Description: u.Description,
		Paypal:      u.Paypal,
		Instagram:   u.Instagram,
		Twitter:     u.Twitter,
		Created_at:  u.Created_at,
		Updated_at:  u.Updated_at,
		Deleted_at:  u.Deleted_at,
	}
}
