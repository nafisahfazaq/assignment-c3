package handler

import (
	"fmt"
	"golang-api/order"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	//"gitlab.com/nafisahfazaq/assignment-c3/-/tree/main/endpoint-golang/order"
)

type orderHandler struct {
	orderService order.Service
}

func NewOrderHandler(orderService order.Service) *orderHandler {
	return &orderHandler{orderService}
}

// endpoint get order list
func (handler *orderHandler) GetOrders(c *gin.Context) {

	orders, err := handler.orderService.FindAll()
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err,
		})
		return
	}

	//endpoint get order list #2
	var ordersResponse []order.OrderResponse
	for _, o := range orders {
		orderResponse := convertToOrderResponse(o)
		ordersResponse = append(ordersResponse, orderResponse)
	}

	c.JSON(http.StatusOK, gin.H{
		"data": ordersResponse,
	})
}

// endpoint get single order
func (handler *orderHandler) GetOrder(c *gin.Context) {
	idString := c.Param("id")
	id, _ := strconv.Atoi(idString)

	o, err := handler.orderService.FindByID(int(id))

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err,
		})
		return
	}

	orderResponse := convertToOrderResponse(o)

	c.JSON(http.StatusOK, gin.H{
		"data": orderResponse,
	})
}

// endpoint update
func (handler *orderHandler) UpdateOrder(c *gin.Context) {
	var orderInput order.OrderInput

	err := c.ShouldBindJSON(&orderInput)
	if err != nil {
		//data validation

		errorMessages := []string{}
		for _, e := range err.(validator.ValidationErrors) {
			errorMessage := fmt.Sprintf("Error on field %s, condition %s", e.Field(), e.ActualTag())
			errorMessages = append(errorMessages, errorMessage)
		}

		c.JSON(http.StatusBadRequest, gin.H{
			"error": errorMessages,
		})
		return
	}

	idString := c.Param("id")
	id, _ := strconv.Atoi(idString)
	order, err := handler.orderService.Update(id, orderInput)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err,
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"data": convertToOrderResponse(order),
	})
}

// endpoint delete
func (handler *orderHandler) DeleteOrder(c *gin.Context) {
	idString := c.Param("id")
	id, _ := strconv.Atoi(idString)
	o, err := handler.orderService.Delete(int(id))

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err,
		})
		return
	}

	orderResponse := convertToOrderResponse(o)

	c.JSON(http.StatusOK, gin.H{
		"data": orderResponse,
	})
}

// add data
func (handler *orderHandler) CreateOrder(c *gin.Context) {
	var orderInput order.OrderInput

	err := c.ShouldBindJSON(&orderInput)
	if err != nil {
		//data validation

		errorMessages := []string{}
		for _, e := range err.(validator.ValidationErrors) {
			errorMessage := fmt.Sprintf("Error on field %s, condition %s", e.Field(), e.ActualTag())
			errorMessages = append(errorMessages, errorMessage)
		}

		c.JSON(http.StatusBadRequest, gin.H{
			"error": errorMessages,
		})
		return
	}

	//call the service
	order, err := handler.orderService.Create(orderInput)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err,
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"data": convertToOrderResponse(order),
	})
}

func convertToOrderResponse(o order.Order) order.OrderResponse {
	return order.OrderResponse{
		ID:              o.ID,
		ID_Artist:       o.ID_Artist,
		ID_Commissioner: o.ID_Commissioner,
		ID_Subtype:      o.ID_Subtype,
		Payment_proof:   o.Payment_proof,
		Status:          o.Status,
		Date_Ordered:    o.Date_Ordered,
		Updated_at:      o.Updated_at,
		Date_Finished:   o.Date_Finished,
		Deleted_at:      o.Deleted_at,
	}
}
