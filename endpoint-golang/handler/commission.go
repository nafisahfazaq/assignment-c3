package handler

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"

	//"gitlab.com/nafisahfazaq/assignment-c3/-/tree/main/endpoint-golang/commission"
	"golang-api/commission"
)

type commissionHandler struct {
	commissionService commission.Service
}

func NewCommissionHandler(commissionService commission.Service) *commissionHandler {
	return &commissionHandler{commissionService}
}

// endpoint get commission list
func (handler *commissionHandler) GetCommissions(c *gin.Context) {

	commissions, err := handler.commissionService.FindAll()
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err,
		})
		return
	}

	//endpoint get commissionlist #2
	var commissionsResponse []commission.CommissionResponse
	for _, c := range commissions {
		commissionResponse := convertToCommissionResponse(c)
		commissionsResponse = append(commissionsResponse, commissionResponse)
	}

	c.JSON(http.StatusOK, gin.H{
		"data": commissionsResponse,
	})
}

// endpoint get single commission
func (handler *commissionHandler) GetCommission(c *gin.Context) {
	idString := c.Param("id")
	id, _ := strconv.Atoi(idString)

	comms, err := handler.commissionService.FindByID(int(id))

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err,
		})
		return
	}

	commissionResponse := convertToCommissionResponse(comms)

	c.JSON(http.StatusOK, gin.H{
		"data": commissionResponse,
	})
}

// endpoint update
func (handler *commissionHandler) UpdateCommission(c *gin.Context) {
	var commissionInput commission.CommissionInput

	err := c.ShouldBindJSON(&commissionInput)
	if err != nil {
		//data validation

		errorMessages := []string{}
		for _, e := range err.(validator.ValidationErrors) {
			errorMessage := fmt.Sprintf("Error on field %s, condition %s", e.Field(), e.ActualTag())
			errorMessages = append(errorMessages, errorMessage)
		}

		c.JSON(http.StatusBadRequest, gin.H{
			"error": errorMessages,
		})
		return
	}

	idString := c.Param("id")
	id, _ := strconv.Atoi(idString)
	commission, err := handler.commissionService.Update(id, commissionInput)

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err,
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"data": convertToCommissionResponse(commission),
	})
}

// endpoint delete
func (handler *commissionHandler) DeleteCommission(c *gin.Context) {
	idString := c.Param("id")
	id, _ := strconv.Atoi(idString)
	comms, err := handler.commissionService.Delete(int(id))

	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err,
		})
		return
	}

	commissionResponse := convertToCommissionResponse(comms)

	c.JSON(http.StatusOK, gin.H{
		"data": commissionResponse,
	})
}

// add data
func (handler *commissionHandler) CreateCommission(c *gin.Context) {
	var commissionInput commission.CommissionInput

	err := c.ShouldBindJSON(&commissionInput)
	if err != nil {
		//data validation

		errorMessages := []string{}
		for _, e := range err.(validator.ValidationErrors) {
			errorMessage := fmt.Sprintf("Error on field %s, condition %s", e.Field(), e.ActualTag())
			errorMessages = append(errorMessages, errorMessage)
		}

		c.JSON(http.StatusBadRequest, gin.H{
			"error": errorMessages,
		})
		return
	}

	//call the service
	commission, err := handler.commissionService.Create(commissionInput)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err,
		})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"data": convertToCommissionResponse(commission),
	})
}

func convertToCommissionResponse(c commission.Commission) commission.CommissionResponse {
	return commission.CommissionResponse{
		ID:         c.ID,
		ID_User:    c.ID_User,
		Image_URL:  c.Image_URL,
		Title:      c.Title,
		Style:      c.Style,
		Created_at: c.Created_at,
		Updated_at: c.Updated_at,
		Deleted_at: c.Deleted_at,
	}
}
