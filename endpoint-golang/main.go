package main

import (
	"log"

	"golang-api/commission"
	"golang-api/handler"
	"golang-api/order"
	"golang-api/subtype"
	"golang-api/user"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"

	"github.com/gin-gonic/gin"
)

func main() {
	//RE-CHECK HERE
	dsn := "root:@tcp(127.0.0.1:3306)/endpoint-golang?charset=utf8mb4&parseTime=True&loc=Local"
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})

	if err != nil {
		log.Fatal("DB connection error")
	}

	db.AutoMigrate((&user.User{}))

	router := gin.Default()

	//versioning
	v1 := router.Group("/v1")

	//user
	userRepository := user.NewRepository(db)
	userService := user.NewService(userRepository)
	userHandler := handler.NewUserHandler(userService)

	//RE-CHECK endpoint
	v1.GET("/users", userHandler.Getusers)
	v1.GET("/users/:id", userHandler.GetUser)
	v1.POST("/users", userHandler.CreateUser)
	v1.PUT("/users/:id", userHandler.UpdateUser)
	v1.DELETE("/users/:id", userHandler.DeleteUser)

	//commission
	commissionRepository := commission.NewRepository(db)
	commissionService := commission.NewService(commissionRepository)
	commissionHandler := handler.NewCommissionHandler(commissionService)

	//RE-CHECK endpoint
	v1.GET("/commissions", commissionHandler.GetCommissions)
	v1.GET("/commissions/:id", commissionHandler.GetCommission)
	v1.POST("/commissions", commissionHandler.CreateCommission)
	v1.PUT("/commissions/:id", commissionHandler.UpdateCommission)
	v1.DELETE("/commissions/:id", commissionHandler.DeleteCommission)

	//subtype
	subtypeRepository := subtype.NewRepository(db)
	subtypeService := subtype.NewService(subtypeRepository)
	subtypeHandler := handler.NewSubtypeHandler(subtypeService)

	//RE-CHECK endpoint
	v1.GET("/subtypes", subtypeHandler.GetSubtypes)
	v1.GET("/subtypes/:id", subtypeHandler.GetSubtype)
	v1.POST("/subtypes", subtypeHandler.CreateSubtype)
	v1.PUT("/subtypes/:id", subtypeHandler.UpdateSubtype)
	v1.DELETE("/subtypes/:id", subtypeHandler.DeleteSubtype)

	//order
	orderRepository := order.NewRepository(db)
	orderService := order.NewService(orderRepository)
	orderHandler := handler.NewOrderHandler(orderService)

	//RE-CHECK endpoint
	v1.GET("/orders", orderHandler.GetOrders)
	v1.GET("/orders/:id", orderHandler.GetOrder)
	v1.POST("/orders", orderHandler.CreateOrder)
	v1.PUT("/orders/:id", orderHandler.UpdateOrder)
	v1.DELETE("/orders/:id", orderHandler.DeleteOrder)

	// listen and serve on localhost:8080
	router.Run(":8080")
}
