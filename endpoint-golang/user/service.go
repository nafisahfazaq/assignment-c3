package user

type Service interface {
	FindAll() ([]User, error)
	FindByID(ID int) (User, error)
	Create(userInput UserInput) (User, error)
	Update(ID int, userInput UserInput) (User, error)
	Delete(ID int) (User, error)
}

type service struct {
	repository Repository
}

// new service
func NewService(repository Repository) *service {
	return &service{repository}
}

// read data
func (s *service) FindAll() ([]User, error) {
	users, err := s.repository.FindAll()
	return users, err
}

// search data
func (s *service) FindByID(ID int) (User, error) {
	user, err := s.repository.FindByID(ID)
	return user, err
}

// create data
func (s *service) Create(userInput UserInput) (User, error) {
	user := User{
		ID:          userInput.ID,
		Name:        userInput.Name,
		Email:       userInput.Email,
		Password:    userInput.Password,
		Description: userInput.Description,
		Paypal:      userInput.Paypal,
		Instagram:   userInput.Instagram,
		Twitter:     userInput.Twitter,
		Created_at:  userInput.Created_at,
		Updated_at:  userInput.Updated_at,
		Deleted_at:  userInput.Deleted_at,
	}

	newUser, err := s.repository.Create(user)
	return newUser, err
}

// update data
func (s *service) Update(ID int, userInput UserInput) (User, error) {
	user, _ := s.repository.FindByID(ID)

	user.Name = userInput.Name
	user.Email = userInput.Email
	user.Password = userInput.Password
	user.Description = userInput.Description
	user.Paypal = userInput.Paypal
	user.Instagram = userInput.Instagram
	user.Twitter = userInput.Twitter
	user.Created_at = userInput.Created_at
	user.Updated_at = userInput.Updated_at
	user.Deleted_at = userInput.Deleted_at

	newUser, err := s.repository.Update(user)
	return newUser, err
}

// update data
func (s *service) Delete(ID int) (User, error) {
	user, _ := s.repository.FindByID(ID)
	newUser, err := s.repository.Delete(user)
	return newUser, err
}
