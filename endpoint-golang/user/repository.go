package user

import "gorm.io/gorm"

type Repository interface {
	FindAll() ([]User, error)
	FindByID(ID int) (User, error)
	Create(user User) (User, error)
	Update(user User) (User, error)
	Delete(user User) (User, error)
}

// implementation (private so the title lowcase)
type repository struct {
	db *gorm.DB
	//change the database
}

// new repo
func NewRepository(db *gorm.DB) *repository {
	return &repository{db}
}

// read data
func (r *repository) FindAll() ([]User, error) {
	var users []User
	err := r.db.Find(&users).Error
	return users, err
}

// search data
func (r *repository) FindByID(ID int) (User, error) {
	var user User
	err := r.db.Find(&user, ID).Error
	return user, err
}

// create data
func (r *repository) Create(user User) (User, error) {
	err := r.db.Create(&user).Error
	return user, err
}

// update data
func (r *repository) Update(user User) (User, error) {
	err := r.db.Save(&user).Error
	return user, err
}

// delete data
func (r *repository) Delete(user User) (User, error) {
	err := r.db.Delete(&user).Error
	return user, err
}
