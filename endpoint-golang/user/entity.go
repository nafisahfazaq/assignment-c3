package user

import "time"

type User struct {
	ID          int
	Name        string
	Email       string
	Password    string
	Description string
	Paypal      string
	Instagram   string
	Twitter     string
	Created_at  time.Time
	Updated_at  time.Time
	Deleted_at  time.Time
}
