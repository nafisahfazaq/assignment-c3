package user

import "time"

// struct for post user
type UserInput struct {
	ID          int
	Name        string `json:"name" binding:"required"`
	Email       string `json:"email" binding:"required"`
	Password    string `json:"password" binding:"required"`
	Description string
	Paypal      string
	Instagram   string
	Twitter     string
	Created_at  time.Time
	Updated_at  time.Time
	Deleted_at  time.Time
}
